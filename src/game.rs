use near_rng::Rng;
use near_sdk::borsh::{self, BorshDeserialize, BorshSerialize};
use near_sdk::{env, log, AccountId, Balance};

#[derive(Debug, BorshDeserialize, BorshSerialize)]
pub enum GameState {
    Created,
    Playing,
    Ended,
}

#[derive(Debug, BorshDeserialize, BorshSerialize)]
pub enum TypeCard {
    Metal,
    Wood,
    Water,
    Fire,
    Earth,
}

impl TypeCard {
    pub fn random_type() -> TypeCard {
        let mut rng = Rng::new(&env::random_seed());
        let value = rng.rand_range_u32(0, 5);
        let _typeCard;
        println!("random {}", value);
        log!("randomsss {}", value);

        match value {
            0 => _typeCard = TypeCard::Metal,
            1 => _typeCard = TypeCard::Wood,
            2 => _typeCard = TypeCard::Water,
            3 => _typeCard = TypeCard::Fire,
            4 => _typeCard = TypeCard::Earth,
            _ => panic!("random not found"),
        }
        _typeCard
    }
}

#[derive(Debug, BorshDeserialize, BorshSerialize)]
pub struct Index {
    Attack: f32,
    Defence: f32,
}

impl Index {
    pub fn new() -> Self {
        Self {
            Attack: 1.0,
            Defence: 1.0,
        }
    }
}

#[derive(Debug, BorshDeserialize, BorshSerialize)]
pub struct Player {
    account: AccountId,
    index: Index,
    type_card: TypeCard,
}

impl Player {
    pub fn new(accountid: AccountId) -> Self {
        Self {
            account: accountid,
            index: Index::new(),
            type_card: TypeCard::random_type(),
        }
    }
    pub fn set_index(&mut self, attack: f32, defence: f32) {
        self.index.Attack = attack;
        self.index.Defence = defence;
    }

    pub fn get_attack(&self) -> f32 {
        self.index.Attack
    }

    pub fn get_defence(&self) -> f32 {
        self.index.Defence
    }

    pub fn upgrade(&mut self) {
        self.index.Attack += 1.0;
        self.index.Defence += 1.0;
    }
}

#[derive(Debug, BorshDeserialize, BorshSerialize)]
pub struct Game {
    pub game_state: GameState,
    pub player_1: u8,
    pub player_2: Option<u8>,
    pub isMeTurn: bool,
    pub deposit: Balance,
    pub winner: Option<u8>,
}

impl Game {
    pub fn get_turn(&self) -> bool {
        self.isMeTurn
    }

    pub fn new_game(amount: Balance, idPlayer: u8) -> Game {
        Game {
            game_state: GameState::Created,
            player_1: idPlayer,
            player_2: None,
            isMeTurn: false,
            deposit: amount,
            winner: None,
        }
    }
}
