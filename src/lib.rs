use near_sdk::borsh::{self, BorshDeserialize, BorshSerialize};
use near_sdk::collections::UnorderedMap;
use near_sdk::{env, log, AccountId, Balance, BorshStorageKey};

mod game;
use game::*;

const ONE_NEAR: Balance = 1_000_000_000_000_000_000_000_000;
const ONE_SECOND: u64 = 1_000_000_000;
const FIX_PERSENT: f32 = 0.3;

#[derive(BorshStorageKey, BorshSerialize)]
pub enum StorageKeys {
    Games,
    Players,
}

pub struct Contract {
    games: UnorderedMap<u8, Game>,
    next_game_id: u8,
    players: UnorderedMap<u8, Player>,
    next_player_id: u8,
}

impl Contract {
    pub fn new() -> Self {
        assert!(!env::state_exists(), "Contract is ready");
        log!("contract initialize");
        Self {
            games: UnorderedMap::new(StorageKeys::Games),
            next_game_id: 0,
            players: UnorderedMap::new(StorageKeys::Players),
            next_player_id: 0,
        }
    }

    pub fn create_player(&mut self, accountid: AccountId) -> u8 {
        // still create player free
        self.next_player_id += 1;
        let player = Player::new(accountid);
        self.players.insert(&self.next_player_id, &player);
        self.next_player_id
    }

    pub fn get_player(&self, playerid: u8) {
        assert!(
            self.players.get(&playerid).is_some(),
            "Playerid is not found"
        );
        println!("get player {:?}", self.players.get(&playerid));
    }

    pub fn new_game(&mut self, idPlayer: u8) -> u8 {
        let amount = env::attached_deposit();
        assert!(amount >= ONE_NEAR, "amount >= 1 Near");
        assert!(
            self.players.get(&idPlayer).is_some(),
            "Not have this player"
        );

        self.next_game_id += 1;
        let game = Game::new_game(ONE_NEAR, idPlayer);
        self.games.insert(&self.next_game_id, &game);
        self.next_game_id
    }

    pub fn get_game(&self, gameid: u8) {
        assert!(self.games.get(&gameid).is_some(), "gameid is not found");
        println!("game {:?}", self.games.get(&gameid));
    }

    pub fn join_game(&mut self, gameid: u8, idPlayer: u8) {
        assert!(self.games.get(&gameid).is_some(), "gameid is not found");
        assert!(
            self.players.get(&idPlayer).is_some(),
            "Not have this player"
        );
        let amount = env::attached_deposit();
        assert!(amount >= ONE_NEAR, "amount >= 1 Near");
        let mut game = self.games.get(&gameid).unwrap();
        game.game_state = GameState::Playing;
        game.player_2 = Some(idPlayer);
        game.deposit += amount;
        self.games.insert(&gameid, &game);
    }

    pub fn fight_card(&mut self, gameid: u8) {
        assert!(self.games.get(&gameid).is_some(), "gameid is not found");
        let mut game = self.games.get(&gameid).unwrap();
        assert!(game.player_2.is_some(), "player2 is not found");

        assert!(
            self.players.get(&game.player_1).is_some()
                && self.players.get(&game.player_2.unwrap()).is_some(),
            "player 1 or 2 not found"
        );

        let mut player1 = self.players.get(&game.player_1).unwrap();
        let mut player2 = self.players.get(&game.player_2.unwrap()).unwrap();
        let mut player1_att = player1.get_attack();
        let mut player1_def = player1.get_defence();
        let mut player2_att = player2.get_attack();
        let mut player2_def = player2.get_defence();

        println!(
            "fight {:?} {:?} {} {}",
            player1_att, player1_def, player2_att, player2_def
        );

        // check result
        if player2.get_attack() == player1.get_attack()
            && player2.get_defence() == player1.get_defence()
        {
            println!("Draw!");
        }

        if player1.get_attack() > player2.get_attack()
            && player1.get_defence() > player2.get_defence()
        {
            println!("Player 1 Win!");
            player1_att += player2_att * FIX_PERSENT;
            player1_def += player2_def * FIX_PERSENT;
            player2_att -= player1_att * FIX_PERSENT;
            player2_def -= player1_def * FIX_PERSENT;

            game.winner = Some(1);
        }
        if player2.get_attack() > player1.get_attack()
            && player2.get_defence() > player1.get_defence()
        {
            println!("Player 2 Win!");
            player2_att += player1_att * FIX_PERSENT;
            player2_def += player1_def * FIX_PERSENT;
            player1_att -= player2_att * FIX_PERSENT;
            player1_def -= player2_def * FIX_PERSENT;

            game.winner = Some(2);
        }
        if player1.get_attack() > player2.get_attack()
            && player1.get_defence() < player2.get_defence()
        {
            println!("Draw!");
            player1_att += player2_att * FIX_PERSENT;
            player1_def -= player2_def * FIX_PERSENT;
            player2_att -= player1_att * FIX_PERSENT;
            player2_def += player1_def * FIX_PERSENT;
        }
        if player2.get_attack() > player1.get_attack()
            && player2.get_defence() < player1.get_defence()
        {
            println!("Draw!");
            player1_att -= player2_att * FIX_PERSENT;
            player1_def += player2_def * FIX_PERSENT;
            player2_att += player1_att * FIX_PERSENT;
            player2_def -= player1_def * FIX_PERSENT;
        }

        game.game_state = GameState::Ended;
        println!(
            "Done fight game! {} {} {} {}",
            player1_att, player1_def, player2_att, player2_def
        );
        player1.set_index(player1_att, player1_def);
        player2.set_index(player2_att, player2_def);

        self.players.insert(&game.player_1, &player1);
        self.players.insert(&game.player_2.unwrap(), &player2);

        self.games.insert(&gameid, &game);
    }

    pub fn cluster_upgrade_info_player(&mut self, playerid: u8) {
        // player need buy cluster & consuming some time to come back
        // pay for admin a little money
        assert!(self.players.get(&playerid).is_some(), "Playerid not found");
        let mut _player = self.players.get(&playerid).unwrap();
        _player.upgrade();

        self.players.insert(&playerid, &_player);
    }

    pub fn leave_game(&mut self, gameid: u8, id_player2: u8) {
        assert!(self.games.get(&gameid).is_some(), "gameid not found");
        assert!(
            self.players.get(&id_player2).is_some(),
            "Playerid not found"
        );

        let mut game = self.games.get(&gameid).unwrap();
        assert!(game.player_2.unwrap() == id_player2, "Your not is player2");
        game.game_state = GameState::Created;
        game.player_2 = None;
        game.winner = None;

        self.games.insert(&gameid, &game);
    }

    pub fn player_mate_children() {
        // need buy room & waiting other player join mate
        // afterward 2s output a children have some index has gen both father & mother
    }
}

#[cfg(test)]
mod tests {
    use near_sdk::test_utils::{accounts, VMContextBuilder};
    use near_sdk::{testing_env, MockedBlockchain};

    use super::*;

    #[test]
    pub fn test_new_game() {
        let mut context = VMContextBuilder::new();
        testing_env!(context.predecessor_account_id(accounts(0)).build());
        context.attached_deposit(1_000_000_000_000_000_000_000_000);
        testing_env!(context.predecessor_account_id(accounts(0)).build());
        let mut contract = Contract::new();
        contract.create_player(accounts(0).to_string());
        contract.create_player(accounts(1).to_string());
        contract.get_player(1);
        contract.get_player(2);

        contract.cluster_upgrade_info_player(2);

        contract.new_game(1);
        contract.get_game(1);

        contract.join_game(1, 2);
        contract.get_game(1);

        contract.fight_card(1);

        contract.get_game(1);

        contract.get_player(1);
        contract.get_player(2);

        contract.get_game(1);
        contract.leave_game(1, 2);

        contract.get_game(1);
    }
}
